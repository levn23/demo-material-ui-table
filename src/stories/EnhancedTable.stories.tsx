import React from "react";
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Story, Meta } from "@storybook/react/types-6-0";

import { EnhancedTable, EnhancedTableProps } from "../table";
import { data } from "../data";
import { columns } from "../columns";
import { Order } from "../App";

export default {
  title: "Example/EnhancedTable",
  component: EnhancedTable,
} as Meta;

const Template = <T extends object>(): Story<EnhancedTableProps<T>> => (
  args
) => <EnhancedTable {...args} />;

export const Default = Template<Order>().bind({});
Default.args = {
  columns,
  rows: data,
};

export const FixedHeight = Template<Order>().bind({});
FixedHeight.storyName = "Fixed Height";
FixedHeight.args = {
  columns,
  rows: data,
  options: {
    height: "200px",
  },
};

export const MultipleSelection = Template<Order>().bind({});
MultipleSelection.storyName = "Multiple Selection";
MultipleSelection.args = {
  columns,
  rows: data,
  options: {
    multipleSelection: true,
  },
};

export const SingleSelection = Template<Order>().bind({});
SingleSelection.storyName = "Signle Selection";
SingleSelection.args = {
  columns,
  rows: data,
  options: {
    onRowClick: (evt, row) => {
      alert(JSON.stringify(row));
    },
  },
};

export const DoubleClick = Template<Order>().bind({});
DoubleClick.storyName = "Row Double Click";
DoubleClick.args = {
  columns,
  rows: data,
  options: {
    onDoubleRowClick: (evt, row) => {
      alert(JSON.stringify(row));
    },
  },
};

export const Pagination = Template<Order>().bind({});
Pagination.storyName = "Pagination";
Pagination.args = {
  columns,
  rows: data,
  options: {
    rowsPerPage: 5,
    rowsPerPageOptions: [5, 10, 15],
  },
};

export const InlineEdit = Template<Order>().bind({});
InlineEdit.storyName = "🚧 Inline Edit";
InlineEdit.args = {
  columns,
  rows: data,
};
