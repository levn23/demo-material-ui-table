import { TableRow, TableRowProps } from "@material-ui/core";
import React, { forwardRef } from "react";

interface EnhancedTableRowProps extends TableRowProps {}

export const EnhancedTableRow = forwardRef(
  (props: EnhancedTableRowProps, ref: any) => {
    return (
      <TableRow
        {...props}
        ref={ref}
        hover
        // style={onRowClick ? { cursor: "pointer" } : {}}
        tabIndex={-1}
      />
    );
  }
);
