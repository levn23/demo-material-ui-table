import { makeStyles, TableFooterProps } from "@material-ui/core";
import React from "react";

interface EnhancedTableFooterProps extends TableFooterProps {
  add: boolean;
  addRowEnabled?: boolean;
  deleteRowEnabled?: boolean;
  handleAddRow?: () => void;
  handleCancelAddRow: () => void;
}

const useStyle = makeStyles({
  footer: {
    display: "flex",
    borderTop: "1px solid rgba(224,224,224,1)",
    borderBottom: "1px solid rgba(224,224,224,1)"
  },
  addRowText: {
    fontSize: "0.7rem",
    fontWeight: "bold",
    fontStyle: "italic"
  }
});

export const EnhancedTableFooter = ({
  add,
  addRowEnabled,
  deleteRowEnabled,
  handleAddRow,
  handleCancelAddRow
}: EnhancedTableFooterProps) => {
  const classes = useStyle();
  return (
    <div className={classes.footer}>
      <span className={classes.addRowText}>
        {addRowEnabled && "<Ctrl+Insert> to add record; "}
        {deleteRowEnabled && "<Ctrl+Delete> to delete record"}
      </span>
      {add && (
        <div style={{ marginLeft: "auto" }}>
          <button onClick={handleAddRow}>OK</button>
          <button onClick={handleCancelAddRow}>Cancel</button>
        </div>
      )}
    </div>
  );
};
