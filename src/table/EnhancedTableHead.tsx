import {
  TableHead,
  TableRow,
  TableCell,
  Checkbox,
  TableSortLabel
} from "@material-ui/core";
import { ClassNameMap } from "@material-ui/core/styles/withStyles";
import React from "react";
import { InnerColumn, Options, OrderBy } from ".";

interface EnhancedTableHeadProps<T> {
  classes: ClassNameMap<any>;
  innerColumns: InnerColumn<T>[];
  rowsCount: number;
  options?: Options<T>;
  selectedTableDataIds: number[];
  handleItemSelectAll: (selectAll: boolean) => void;
  handleSort: (targetField: string) => void;
  orderBy: OrderBy;
  tableEditable: boolean;
}

export const EnhancedTableHead = <T extends object>({
  classes,
  innerColumns,
  rowsCount,
  options,
  selectedTableDataIds,
  handleItemSelectAll,
  handleSort,
  orderBy,
  tableEditable
}: EnhancedTableHeadProps<T>) => {
  const { multipleSelection } = options || {};
  return (
    <TableHead>
      <TableRow>
        {multipleSelection && (
          <TableCell padding="checkbox">
            <Checkbox
              checked={
                rowsCount > 0 && selectedTableDataIds.length === rowsCount
              }
              onChange={evt => handleItemSelectAll(evt.target.checked)}
              size="small"
            />
          </TableCell>
        )}
        {innerColumns.map((col, idx) => (
          <TableCell
            key={idx}
            className={classes.cell}
            align={col.align ? col.align : "left"}
            style={{ width: col.width }}
          >
            {col.sortable === false || tableEditable ? (
              col.title
            ) : (
              <TableSortLabel
                active={col.field === orderBy.field}
                direction={orderBy.order ? orderBy.order : undefined}
                onClick={() => handleSort(col.field)}
              >
                {col.title}
              </TableSortLabel>
            )}
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
};
