import { TableCell, TableCellProps } from "@material-ui/core";
import { ClassNameMap } from "@material-ui/core/styles/withStyles";
import React from "react";
import { Column } from ".";
import { EnhancedTableEditField } from "./EnhancedTableEditField";

interface EnhancedTableAddCellProps<T> extends TableCellProps {
  classes: ClassNameMap<any>;
  column: Column<T>;
  onEditFieldBlur?: (updatedValue: any) => void;
  autoFocus?: boolean;
}

export const EnhancedTableAddCell = <T extends object>({
  classes,
  column,
  onEditFieldBlur,
  autoFocus,
  ...props
}: EnhancedTableAddCellProps<T>) => (
  <TableCell
    {...props}
    className={column.editable ? classes.editCell : classes.cell}
    align={column.align}
  >
    {column.editable ? (
      <EnhancedTableEditField
        type={column.type}
        value={undefined}
        editFormat={column.editFormat}
        selectOptions={column.selectOptions}
        onBlur={onEditFieldBlur!}
        autoFocus={autoFocus}
      />
    ) : column.defaultEditValue === undefined ? null : (
      column.defaultEditValue + ""
    )}
  </TableCell>
);
