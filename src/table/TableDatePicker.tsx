import { TextField, TextFieldProps, withStyles } from "@material-ui/core";
import {
  KeyboardDatePicker,
  KeyboardDatePickerProps
} from "@material-ui/pickers";
import moment from "moment";
import React, { forwardRef, useMemo, useRef, useState } from "react";

const InnerTextField = forwardRef((props: TextFieldProps, ref: any) => {
  return (
    <TextField
      inputRef={ref}
      {...props}
      variant="outlined"
      size="small"
      fullWidth
    />
  );
});

const StyledInnerTextField = withStyles({
  root: {
    "& input": {
      padding: "5px 0 5px 8px",
      fontSize: "0.875rem"
    },
    "& .MuiInputAdornment-positionEnd": {
      marginLeft: "2px"
    },
    "& .MuiInputAdornment-positionEnd > button": {
      padding: "0"
    },
    "& .MuiOutlinedInput-adornedEnd": {
      paddingRight: "2px"
    }
  }
})(InnerTextField);

interface InnerDataPickerProps
  extends Omit<KeyboardDatePickerProps, "onChange" | "onBlur"> {
  onBlur?: (
    event?: React.FocusEvent<HTMLInputElement | HTMLTextAreaElement>,
    data?: string | number | object | Date | null
  ) => void;
}

export const TableDatePicker = ({
  value,
  onBlur,
  onClick,
  ...props
}: InnerDataPickerProps) => {
  const inputRef = useRef<any>(null);
  const [innerVal, setInnerVal] = useState(value);
  const [open, setOpen] = useState(false);

  const focusInput = () => {
    setTimeout(() => inputRef.current?.current?.focus(), 100);
  };

  return (
    <KeyboardDatePicker
      {...props}
      autoOk
      variant="inline"
      value={innerVal || null}
      onClick={evt => {
        evt.stopPropagation();
        onClick && onClick(evt);
      }}
      onChange={date => setInnerVal(date)}
      onClose={() => {
        setOpen(false);
        focusInput();
      }}
      onBlur={evt => {
        if (!open && onBlur) {
          if (innerVal === null || innerVal === undefined) {
            onBlur(evt, innerVal);
            return;
          }

          const validDate = moment(innerVal).isValid();
          validDate && onBlur(evt, moment(innerVal).toDate());
        }
      }}
      onMouseDown={evt => {
        // to prevent onBlur called to early when click calendar button
        const targetNode = (evt.target as any)?.nodeName;
        if (targetNode === "svg" || targetNode === "path") {
          setOpen(true);
        }
      }}
      TextFieldComponent={useMemo(
        () => inputProps => {
          // should not pass custom ref as inputRef to TextField since it is preserved in picker libirary
          if (!inputRef.current) {
            inputRef.current = inputProps.inputRef;
          }
          return <StyledInnerTextField {...inputProps} />;
        },
        []
      )}
      format="DD/MM/YYYY"
    />
  );
};
