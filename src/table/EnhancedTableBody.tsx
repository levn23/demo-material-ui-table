import {
  Checkbox,
  TableBody,
  TableBodyProps,
  TableCell
} from "@material-ui/core";
import { ClassNameMap } from "@material-ui/core/styles/withStyles";
import moment from "moment";
import { get } from "lodash";
import React, { useEffect, useRef } from "react";
import { Edit, InnerColumn, InnerRow } from ".";
import { EnhancedTableRow } from "./EnhancedTableRow";
import { trimTableData } from "./EnhancedTable";
import { EnhancedTableCell } from "./EnhancedTableCell";
import { EnhancedTableAddCell } from "./EnhancedTableAddCell";

interface EnhancedTableBodyProps<T> extends TableBodyProps {
  classes: ClassNameMap<any>;
  innerColumns: InnerColumn<T>[];
  innerRows: InnerRow<T>[];
  selectedTableDataIds: number[];
  onRowClick: (
    event: React.MouseEvent<HTMLTableRowElement, MouseEvent>,
    row: InnerRow<T>
  ) => void;
  onRowDoubleClick?: (
    event: React.MouseEvent<HTMLTableRowElement, MouseEvent>,
    row: InnerRow<T>
  ) => void;
  onCellUpdated?: (
    tableDataId: number,
    field: string,
    updatedValue: any
  ) => void;
  multipleSelection?: boolean;
  isCellEdit: (tableDataId: number, field: string) => boolean | undefined;
  setEdit: (edit?: Edit) => void;
  add: boolean;
  setAdd: (add: boolean) => void;
  setNewRow: React.Dispatch<React.SetStateAction<T | undefined>>;
  updateInnerRows: (row: InnerRow<T>, field: string, newValue: any) => void;
  page: number;
  rowsPerPage: number;
}

export const EnhancedTableBody = <T extends object>({
  classes,
  innerColumns,
  innerRows,
  selectedTableDataIds,
  onRowClick,
  onRowDoubleClick,
  onCellUpdated,
  multipleSelection,
  isCellEdit,
  setEdit,
  add,
  setAdd,
  setNewRow,
  updateInnerRows,
  page,
  rowsPerPage,
  ...props
}: EnhancedTableBodyProps<T>) => {
  const addRowRef = useRef<any>();
  useEffect(() => {
    add &&
      setTimeout(
        () => addRowRef.current?.scrollIntoView({ behavior: "smooth" }),
        200
      );
  }, [add]);

  return (
    <TableBody {...props}>
      {innerRows
        .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
        .map(row => {
          const isItemSelected = selectedTableDataIds.some(
            id => id === row.tableData.id
          );
          return (
            <EnhancedTableRow
              hover
              key={row.tableData.id}
              className={add ? classes.inactiveRow : undefined}
              onClick={evt => onRowClick(evt, row)}
              onDoubleClick={
                onRowDoubleClick ? evt => onRowDoubleClick(evt, row) : undefined
              }
              selected={isItemSelected}
            >
              {multipleSelection && (
                <TableCell padding="checkbox">
                  <Checkbox
                    tabIndex={-1}
                    checked={isItemSelected}
                    size="small"
                  />
                </TableCell>
              )}

              {innerColumns.map((col, idx) => {
                let displayNode: string | React.ReactNode = col.render || "";
                let fieldVal = get(row, col.field);

                if (col.render) {
                  displayNode = col.render(
                    trimTableData(row) as T,
                    row.tableData.id
                  );
                } else if (fieldVal === undefined || fieldVal === null) {
                  displayNode = "";
                } else if (fieldVal instanceof Date) {
                  displayNode = moment(fieldVal).format("DD-MMM-YYYY");
                } else {
                  displayNode = fieldVal + "";
                }

                const isEditing = isCellEdit(row.tableData.id, col.field);

                return (
                  <EnhancedTableCell
                    key={`c_${idx}`}
                    classes={classes}
                    column={col}
                    fieldValue={fieldVal}
                    displayNode={displayNode}
                    isEditing={isEditing}
                    onCellDoubleClick={
                      !add && col.editable
                        ? e => {
                            e.stopPropagation();
                            setEdit({
                              tableDataId: row.tableData.id,
                              field: col.field
                            });
                          }
                        : undefined
                    }
                    onEditFieldBlur={
                      col.editable
                        ? updatedValue => {
                            // TODO: check same val
                            if (onCellUpdated) {
                              const newValue = col.editValue
                                ? col.editValue(updatedValue)
                                : updatedValue;

                              updateInnerRows(row, col.field, newValue);

                              onCellUpdated(
                                row.tableData.id,
                                col.field,
                                newValue
                              );
                            }

                            setEdit(undefined);
                          }
                        : undefined
                    }
                  />
                );
              })}
            </EnhancedTableRow>
          );
        })}

      {/* Add row */}
      {add && (
        <EnhancedTableRow ref={addRowRef}>
          {multipleSelection && <TableCell padding="checkbox" />}
          {innerColumns.map((col, idx) => {
            return (
              <EnhancedTableAddCell
                key={`ca_${idx}`}
                classes={classes}
                column={col}
                onEditFieldBlur={updatedValue => {
                  // TODO
                  setNewRow(newRow => {
                    const newValue = col.editValue
                      ? col.editValue(updatedValue)
                      : updatedValue;

                    const newRowCopy = {
                      ...newRow,
                      [col.field]: newValue
                    } as T;
                    return newRowCopy;
                  });
                }}
              />
            );
          })}
        </EnhancedTableRow>
      )}
    </TableBody>
  );
};
