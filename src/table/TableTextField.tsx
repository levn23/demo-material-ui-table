import { TextField, TextFieldProps, withStyles } from "@material-ui/core";
import React, { useState } from "react";

interface InnerTextFieldProps {
  editFormat?: RegExp;
}

const InnerTextField = ({
  editFormat,
  value,
  onClick,
  ...props
}: InnerTextFieldProps & TextFieldProps) => {
  const [innerVal, setInnerVal] = useState(value);
  return (
    <TextField
      {...props}
      value={innerVal === null || innerVal === undefined ? "" : innerVal}
      onClick={evt => {
        evt.stopPropagation();
        onClick && onClick(evt);
      }}
      onChange={evt => {
        const text = evt.target.value;
        let valid = true;
        if (text && editFormat && !editFormat.test(text)) {
          valid = false;
        }
        valid && setInnerVal(evt.target.value);
      }}
      variant="outlined"
      size="small"
      fullWidth
    />
  );
};

export const TableTextField = withStyles({
  root: {
    "& input": {
      padding: "5px 8px",
      fontSize: "0.875rem"
    }
  }
})(InnerTextField);
