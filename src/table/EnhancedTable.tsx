import MomentUtils from "@date-io/moment";
import {
  makeStyles,
  Paper,
  Table,
  TableContainer,
  TablePagination
} from "@material-ui/core";
import { MuiPickersUtilsProvider } from "@material-ui/pickers";
import { get } from "lodash";
import moment from "moment";
import React, { useEffect, useState } from "react";
import { useHotkeys } from "react-hotkeys-hook";
import { EnhancedTableBody } from "./EnhancedTableBody";
import { EnhancedTableFooter } from "./EnhancedTableFooter";
import { EnhancedTableHead } from "./EnhancedTableHead";
import { EnhancedTableProps, InnerRow, Order, OrderBy } from "./index";

export const trimTableData = <T extends object>(d: InnerRow<T>) => {
  const { tableData, ...newData } = d;
  return newData;
};

const comparator = (a: any, b: any) => {
  if (a !== b) {
    // to find nulls
    if (!a) return -1;
    if (!b) return 1;
  }
  return a < b ? -1 : a > b ? 1 : 0;
};

const useStyle = makeStyles(theme => ({
  table: {
    tableLayout: "fixed"
  },
  tableFocused: {
    "&:focus-within": {
      outline: "2px solid " + theme.palette.primary.main
    }
  },
  inactiveRow: {
    opacity: 0.4,
    userSelect: "none"
  },
  cell: {
    padding: "0 4px !important",
    borderLeft: "1px solid rgba(224,224,224,1)"
  },
  editCell: {
    padding: "0 !important",
    borderLeft: "1px solid rgba(224,224,224,1)"
  }
}));

export const EnhancedTable = <T extends object>({
  columns,
  rows,
  options,
  editOptions
}: EnhancedTableProps<T>) => {
  const {
    height,
    multipleSelection,
    onRowClick,
    onDoubleRowClick,
    onSelectionChange,
    rowsPerPage: inRowsPerPage,
    rowsPerPageOptions: inRowsPerPageOptions
  } = options || {};
  const { onRowAdded, onRowDeleted, onCellUpdated } = editOptions || {};
  const classes = useStyle();
  const [innerColumns] = useState([...columns]);
  const [innerRows, setInnerRows] = useState(
    rows.map((x, idx) => ({ ...x, tableData: { id: idx } }))
  );

  const [tableFocused, setTableFocused] = useState(false);
  const [orderBy, setOrderBy] = useState<OrderBy>({});
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(inRowsPerPage ?? 25);
  const [selectedTableDataIds, setSelectedTableDataIds] = useState<number[]>(
    []
  );

  const [add, setAdd] = useState(false);
  const [newRow, setNewRow] = useState<T>();
  const [edit, setEdit] = useState<{ tableDataId: number; field: string }>();

  const tableEditable =
    !!editOptions || innerColumns.some(({ editable }) => editable);

  useHotkeys(
    "ctrl+insert",
    evt => setAdd(add => !add),
    { enabled: !!onRowAdded && tableFocused },
    [tableFocused, onRowAdded]
  );

  useHotkeys(
    "ctrl+delete",
    evt => {
      if (!onRowDeleted) return;

      const targetDeleteRows = innerRows
        .filter(row => selectedTableDataIds.includes(row.tableData.id))
        .map(row => trimTableData(row) as T);
      onRowDeleted(targetDeleteRows, selectedTableDataIds);
      setSelectedTableDataIds([]);
    },
    {
      enabled:
        !!onRowDeleted &&
        !add &&
        !edit &&
        tableFocused &&
        selectedTableDataIds.length >= 1,
      filter: e => {
        // enable short-cut key for checkbox
        !(e.target instanceof HTMLInputElement) &&
          (e.target as HTMLInputElement).type !== "checkbox" &&
          e.preventDefault();

        return true;
      },
      filterPreventDefault: false,
      enableOnTags: ["INPUT"]
    },
    [tableFocused, onRowDeleted, add, edit, selectedTableDataIds, innerRows]
  );

  useEffect(() => {
    // update innerRows when rows are updated outside
    setInnerRows(innerRows => {
      let newInnerRows = [...innerRows];
      // update rows
      newInnerRows.forEach((c, idx, arr) => {
        arr[idx] = { ...rows[c.tableData.id], tableData: c.tableData };
      });

      if (rows.length > innerRows.length) {
        // add rows
        const newAddedRows = rows
          .slice(-(rows.length - innerRows.length)) // get last new records
          .map((r, idx) => ({
            // convert to InnerRow type
            ...r,
            tableData: { id: newInnerRows.length + idx }
          }));
        newInnerRows = [...newInnerRows, ...newAddedRows];
      } else if (rows.length < innerRows.length) {
        // delete rows
        newInnerRows.splice(-(innerRows.length - rows.length));
      }
      return newInnerRows;
    });
  }, [rows]);

  useEffect(() => {
    // update default value for new row
    if (add) {
      const defaultNewRow: any = {};
      innerColumns.forEach(({ field, defaultEditValue }) => {
        defaultNewRow[field] = defaultEditValue;
      });
      setNewRow(defaultNewRow);
    } else {
      setNewRow(undefined);
    }
  }, [add, innerColumns]);

  const handleSort = (targetField: string) => {
    let nextOrder: Order = "asc";
    if (targetField === orderBy.field) {
      if (orderBy.order === "asc") {
        nextOrder = "desc";
      }
    }
    setOrderBy({
      order: nextOrder,
      field: targetField
    });

    // handle sorting
    setInnerRows(innerRows => {
      const newInnerRows = [...innerRows];
      newInnerRows.sort((a, b) => {
        const fieldA = get(a, targetField);
        const fieldB = get(b, targetField);
        let order = 0;
        if (nextOrder === "asc") {
          order = comparator(fieldA, fieldB);
        } else if (nextOrder === "desc") {
          order = comparator(fieldB, fieldA);
        }
        return order;
      });
      return newInnerRows;
    });
  };

  const handleItemSelectAll = (selectAll: boolean) => {
    if (add || edit) return;

    onSelectionChange &&
      onSelectionChange(
        selectAll ? innerRows.map(x => trimTableData(x) as T) : [],
        undefined
      );

    setSelectedTableDataIds(
      selectAll ? innerRows.map(row => row.tableData.id) : []
    );
  };

  const handleItemSelect = (targetTableDataId: number) => {
    if (multipleSelection) {
      // multiple selection
      let checkedTableDataIds: number[] = [];
      if (selectedTableDataIds.some(id => id === targetTableDataId)) {
        // uncheck a row
        checkedTableDataIds = selectedTableDataIds.filter(
          id => id !== targetTableDataId
        );
      } else {
        // check a row
        checkedTableDataIds = [...selectedTableDataIds, targetTableDataId];
      }

      if (onSelectionChange) {
        const targetRow = trimTableData(
          innerRows.find(({ tableData }) => tableData.id === targetTableDataId)!
        ) as T;

        const checkedRows = innerRows
          .filter(({ tableData }) => checkedTableDataIds.includes(tableData.id))
          .map(x => trimTableData(x) as T);

        onSelectionChange(checkedRows, targetRow);
      }

      setSelectedTableDataIds(checkedTableDataIds);
    } else if (onRowClick || onDoubleRowClick || onRowDeleted) {
      // single selection
      setSelectedTableDataIds([targetTableDataId]);
    }
  };

  const handleRowClick = <T extends object>(event: any, row: InnerRow<T>) => {
    if (add || edit) return;

    multipleSelection && handleItemSelect(row.tableData.id);

    if (onRowClick || onRowDeleted) {
      handleItemSelect(row.tableData.id);
      const rowData = trimTableData(row);
      onRowClick && onRowClick(event, rowData as any, row.tableData.id);
    }
  };

  const handleRowDoubleClick = <T extends object>(
    event: any,
    row: InnerRow<T>
  ) => {
    if (!onDoubleRowClick || add || edit) return;

    handleItemSelect(row.tableData.id);
    const rowData = trimTableData(row);
    onDoubleRowClick(event, rowData as any, row.tableData.id);
  };

  const handleChangePage = (evt: any, newPage: number) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (
    evt: React.ChangeEvent<HTMLInputElement>
  ) => {
    setRowsPerPage(parseInt(evt.target.value, 10));
    setPage(0);
  };

  const isCellEdit = (tableDataId: number, field: string) =>
    edit && edit.tableDataId === tableDataId && edit.field === field;

  const updateInnerRows = (row: InnerRow<T>, field: string, newValue: any) => {
    setInnerRows(innerRows => {
      const newInnerRows = [...innerRows];
      const idx = newInnerRows.findIndex(
        r => r.tableData.id === row.tableData.id
      )!;
      newInnerRows[idx] = { ...newInnerRows[idx], [field]: newValue };
      return newInnerRows;
    });
  };

  return (
    <MuiPickersUtilsProvider libInstance={moment} utils={MomentUtils}>
      <Paper
        tabIndex={-1}
        className={classes.tableFocused}
        onFocus={event => setTableFocused(true)}
        onBlur={event => {
          // since clicking sub-elements will always trigger onBlur, check with clicking sub-elements to prevent set state frequently
          if (
            !event.relatedTarget ||
            !event.currentTarget.contains(event.relatedTarget as any)
          ) {
            setTableFocused(false);
          }
        }}
      >
        <TableContainer style={{ height }}>
          <Table className={classes.table} stickyHeader>
            <EnhancedTableHead
              classes={classes}
              innerColumns={innerColumns}
              rowsCount={innerRows.length}
              options={options}
              selectedTableDataIds={selectedTableDataIds}
              handleItemSelectAll={handleItemSelectAll}
              handleSort={handleSort}
              orderBy={orderBy}
              tableEditable={tableEditable}
            />
            <EnhancedTableBody
              classes={classes}
              innerColumns={innerColumns}
              innerRows={innerRows}
              selectedTableDataIds={selectedTableDataIds}
              onRowClick={handleRowClick}
              onRowDoubleClick={
                onDoubleRowClick ? handleRowDoubleClick : undefined
              }
              onCellUpdated={onCellUpdated}
              multipleSelection={multipleSelection}
              isCellEdit={isCellEdit}
              setEdit={setEdit}
              add={add}
              setAdd={setAdd}
              setNewRow={setNewRow}
              updateInnerRows={updateInnerRows}
              page={page}
              rowsPerPage={rowsPerPage}
            />
          </Table>
        </TableContainer>

        {/* TODO */}
        {(onRowAdded || onRowDeleted) && (
          <EnhancedTableFooter
            add={add}
            addRowEnabled={!!onRowAdded}
            deleteRowEnabled={!!onRowDeleted}
            handleAddRow={() => {
              onRowAdded && onRowAdded(newRow || ({} as T));
              setAdd(false);
            }}
            handleCancelAddRow={() => {
              setAdd(false);
            }}
          />
        )}

        <TablePagination
          rowsPerPageOptions={inRowsPerPageOptions ?? [25, 50, 75]}
          component="div"
          count={innerRows.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
      </Paper>
    </MuiPickersUtilsProvider>
  );
};
