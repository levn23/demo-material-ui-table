import React from "react";
import { ColumnType, SelectOption } from ".";
import { TableDatePicker } from "./TableDatePicker";
import { TableSelect } from "./TableSelect";
import { TableTextField } from "./TableTextField";

interface EnhancedTableEditFieldProps {
  type?: ColumnType;
  value: any;
  editFormat?: RegExp;
  selectOptions?: SelectOption[];
  onBlur: (updatedValue: any) => void;
  autoFocus?: boolean;
}

export const EnhancedTableEditField = ({
  type,
  value,
  editFormat,
  selectOptions,
  onBlur,
  autoFocus
}: EnhancedTableEditFieldProps) => {
  if (selectOptions) {
    return (
      <TableSelect
        value={value}
        selectOptions={selectOptions}
        onBlur={onBlur}
        autoFocus={autoFocus}
      />
    );
  }

  switch (type) {
    case "date":
      return (
        <TableDatePicker
          value={value}
          onBlur={(evt, date) => onBlur(date)}
          autoFocus={autoFocus}
        />
      );
    case "number": //todo
    case "string":
    default:
      return (
        <TableTextField
          value={value}
          onBlur={evt => {
            let text: any = evt.target.value;
            // convert string to number type
            if (type === "number") {
              if (text === "") {
                text = undefined;
              } else {
                text = Number(text);
                if (isNaN(text)) {
                  return;
                }
              }
            }
            onBlur(text);
          }}
          editFormat={editFormat}
          autoFocus={autoFocus}
        />
      );
  }
};
