import { TableCell, TableCellProps } from "@material-ui/core";
import { ClassNameMap } from "@material-ui/core/styles/withStyles";
import React from "react";
import { Column } from ".";
import { EnhancedTableEditField } from "./EnhancedTableEditField";

interface EnhancedTableCellProps<T> extends TableCellProps {
  classes: ClassNameMap<any>;
  column: Column<T>;
  fieldValue: any;
  displayNode: React.ReactNode | string;
  isEditing?: boolean;
  onCellDoubleClick?: (
    event: React.MouseEvent<HTMLTableHeaderCellElement, MouseEvent>
  ) => void;
  onEditFieldBlur?: (updatedValue: any) => void;
}

export const EnhancedTableCell = <T extends object>({
  classes,
  column,
  fieldValue,
  displayNode,
  isEditing,
  onCellDoubleClick,
  onEditFieldBlur,
  ...props
}: EnhancedTableCellProps<T>) => {
  return (
    <TableCell
      {...props}
      className={isEditing ? classes.editCell : classes.cell}
      align={column.align}
      onClick={onCellDoubleClick}
    >
      {isEditing ? (
        <EnhancedTableEditField
          type={column.type}
          value={fieldValue}
          editFormat={column.editFormat}
          selectOptions={column.selectOptions}
          onBlur={onEditFieldBlur!}
          autoFocus
        />
      ) : (
        displayNode
      )}
    </TableCell>
  );
};
