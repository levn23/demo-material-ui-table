import { MenuItem, Select, SelectProps, withStyles } from "@material-ui/core";
import React, { FC, useState } from "react";

type SelectOption = {
  label: string;
  value: any;
};

interface TableSelectProps extends Omit<SelectProps, "onBlur"> {
  selectOptions: SelectOption[];
  onBlur: (selectedValue: any) => void;
}

export const InnerTableSelect: FC<TableSelectProps> = ({
  selectOptions,
  value,
  children,
  onClick,
  onBlur,
  ...props
}) => {
  const [innerVal, setInnerVal] = useState(value);
  return (
    <Select
      {...props}
      value={innerVal === undefined || innerVal === null ? "" : innerVal}
      onClick={evt => {
        evt.stopPropagation();
        onClick && onClick(evt);
      }}
      onChange={evt => {
        setInnerVal(evt.target.value);
      }}
      onBlur={evt => onBlur(innerVal)}
      margin="none"
      variant="outlined"
    >
      {selectOptions.map(({ label, value }) => (
        <MenuItem key={value} value={value}>
          {label}
        </MenuItem>
      ))}
    </Select>
  );
};

export const TableSelect = withStyles({
  root: {
    padding: "5px 8px",
    fontSize: "0.875rem"
  }
})(InnerTableSelect);
