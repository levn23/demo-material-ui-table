import React from "react";
import { EnhancedTable } from "./EnhancedTable";

type SelectOption = {
  label: string;
  value: any;
};

type ColumnType = "string" | "number" | "date";

type Column<T> = {
  field: string;
  title: string;
  align?: "left" | "right";
  width?: number;
  type?: ColumnType;
  selectOptions?: SelectOption[];
  sortable?: boolean;

  /**
   * to enable cell to be editable when double click on that cell
   */
  editable?: boolean;

  /**
   * to return a custom update value of edit field after onBlur on edit field
   */
  editValue?: (updatedValue: any) => any;

  /**
   * to retrict cell input format if type is number, string or undefined
   */
  editFormat?: RegExp;

  /**
   * default value on add new row
   */
  defaultEditValue?: any;

  /**
   * to render a custom display value / node for cell
   */
  render?: (row: T, tableDataId: number) => string | React.ReactNode;
};

type Options<T> = {
  /**
   * table height
   */
  height?: string;

  /**
   * to show a checkbox to the leftmost of header and each row
   */
  multipleSelection?: boolean;

  /**
   * to trigger events by signle click on a row
   */
  onRowClick?: (
    event: React.MouseEvent<HTMLTableRowElement, MouseEvent>,
    row: T,
    tableDataId: number
  ) => void;

  /**
   * to trigger events by double click on a row
   */
  onDoubleRowClick?: (
    event: React.MouseEvent<HTMLTableRowElement, MouseEvent>,
    row: T,
    tableDataId: number
  ) => void;

  /**
   * to trigger events if multipleSelection is true
   * @param rows all currently checked rows
   * @param row currently checked/unchecked row. if select "All" checkbox from header, return undefined
   */
  onSelectionChange?: (rows: T[], row?: T) => void;

  /**
   * to show the options of row per page
   * default [25, 50, 75]
   * if rowsPerPageOptions is an empty array, row per page options will be disappered
   */
  rowsPerPageOptions?: number[];

  /**
   * to show number of row(s) in a page
   * default: 25
   */
  rowsPerPage?: number;
};

interface EditOptions<T> {
  /**
   * to enable add row function by short-cut key, and
   * trigger events if add row button is pressed
   */
  onRowAdded?: (row: T) => void;

  /**
   * to enable delete row function, and
   * trigger event if a row is selected and short-cut key is pressed
   */
  onRowDeleted?: (selectedRows: T[], selectedTableDataIds: number[]) => void;

  /**
   * to trigger events after cell updated
   * if cell value are the same as before, this function will not be triggered
   */
  onCellUpdated?: (
    tableDataId: number,
    field: string,
    updatedValue: any
  ) => void;
}

interface EnhancedTableProps<T> {
  columns: Column<T>[];
  rows: T[];
  options?: Options<T>;
  editOptions?: EditOptions<T>; // if editOptions is defined, sorting will not be available
}

/**
 * for inner use
 */
type Order = "asc" | "desc";
type OrderBy = {
  order?: Order;
  field?: string;
};
type Edit = {
  tableDataId: number;
  field: string;
};

type TableData = {
  // to identify row
  id: number;
};

type InnerColumn<T> = Column<T> & {};

type InnerRow<T> = T & {
  tableData: TableData;
};

// for inner use
export type { Order, OrderBy, Edit, TableData, InnerColumn, InnerRow };

export type { SelectOption, ColumnType, Column, EnhancedTableProps, Options };

export { EnhancedTable };
