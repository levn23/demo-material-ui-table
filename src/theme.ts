import { createMuiTheme } from "@material-ui/core";

const Palette = {
  KeyGreen: "#3AB395",
  Green2: "#70D1B2",
  Green3: "#80EACF",
  Green4: "#BBFEED",
  Green5: "#E6FCF6",
  GrayGreen: "#F4F8F7",
  White: "#FFFFFF",
  Black: "#000000",
  Gray1: "#333333",
  Gray2: "#4F4F4F",
  Gray3: "#828282",
  Gray4: "#BDBDBD",
  Gray5: "#E0E0E0",
  Gray6: "#F2F2F2",
  BG: "#F5F5F5",
  NavyBlack: "#3F3D56",
  NavyDarkGray: "#4B486C",
  NavyGray: "#63617C",
  NavyGray2: "#9F9DB9",
  NavyLightGray: "#E0DFEC",
  Red: "#CE5252",
  Coral: "#FF7875",
  LightPink: "#FFCCC7",
  Orange: "#F2994A",
  Warning: "#FACB42",
  Required: "#0077FF"
};

export const theme = createMuiTheme({
  palette: {
    primary: {
      main: Palette.KeyGreen,
      light: Palette.Green3
    },
    secondary: {
      main: Palette.Gray1,
      light: Palette.Green3
    },
    error: {
      main: Palette.Red,
      light: Palette.Coral
    },
    warning: {
      main: Palette.Orange
    }
  },
  props: {
    MuiTable: {
      size: "small"
    },
    MuiTableCell: {
      padding: "none"
    }
  },
  overrides: {
    MuiTableRow: {
      root: {
        "&$selected, &$selected:hover": {
          backgroundColor: Palette.Green5
        }
      }
    },
    MuiTablePagination: {
      toolbar: {
        minHeight: "30px"
      },
      actions: {
        "&& .MuiIconButton-root": {
          padding: "0px 6px"
        }
      }
    }
  }
});
