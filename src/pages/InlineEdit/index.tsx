import React, { useState } from "react";
import { Order } from "../../App";
import { data } from "../../data";
import { EnhancedTable, Column } from "../../table";
import { Prism as SyntaxHighlighter } from "react-syntax-highlighter";
import { dark } from "react-syntax-highlighter/dist/esm/styles/prism";

const code = `
const columns: Column<Order>[] = [
  { field: "id", title: "Id", width: 40 },
  { field: "orderNum", title: "Order Number", editable: true, width: 140 },
  { field: "orderDate", title: "Order Date", type: "date", editable: true, width: 100 },
  {
    field: "price",
    title: "Price",
    type: "number",
    editable: true,
    align: "right",
    editFormat: /^-?(\\d+)?(\\.)?(\\d+)?$/,
    width: 70
  },
  {
    field: "delivered",
    title: "Delivered",
    editable: true,
    selectOptions: [
      { label: "true", value: "true" },
      { label: "false", value: "false" }
    ],
    editValue: val => {
      // convert to boolean type after onBlur on dropdown
      if (typeof val === "boolean") {
        return val;
      }
      return val === "true" ? true : val === "false" ? false : undefined;
    },
    width: 80
  },
  { field: "paid", title: "Paid", editable: false, defaultEditValue: false, width: 45 },
  { field: "remark", title: "Remark", editable: true, width: 700 }
];

export const DefaultTable = () => {
  const [rows, setRows] = useState(
    [
      { id: "1", orderNum: "QMH1001", orderDate: null, price: 11, delivered: false },
      { id: "3",  orderNum: "QMH1003", orderDate: moment("2021-02-22T00:00:00").toDate(), price: 15, delivered: true, remark: "long remark long remark long remark long remark long remark long remark long remark long remark long remark long remark long remark long remark long remark long remark " }
      // ...
    ]
  );

  return (
    <EnhancedTable
      columns={columns}
      rows={rows}
      options={{
        height: "400px",
        multipleSelection: true
      }}
      editOptions={{
        onRowAdded: row => {
          setRows(rows => [...rows, row]);
        },
        onRowDeleted: (selectedRows, selectedTableDataIds) => {
          setRows(rows =>
            rows.filter((row, idx) => !selectedTableDataIds.includes(idx))
          );
        },
        onCellUpdated: (tableDataId, field, updatedValue) => {
          setRows(rows => {
            const newRows = [...rows];
            newRows[tableDataId] = {
              ...newRows[tableDataId],
              [field]: updatedValue
            };
            return newRows;
          });
        }
      }}
    />
  );
}`;

const columns: Column<Order>[] = [
  { field: "id", title: "Id", width: 40 },
  { field: "orderNum", title: "Order Number", editable: true, width: 140 },
  {
    field: "orderDate",
    title: "Order Date",
    type: "date",
    editable: true,
    width: 100
  },
  {
    field: "price",
    title: "Price",
    type: "number",
    editable: true,
    align: "right",
    editFormat: /^-?(\d+)?(\.)?(\d+)?$/,
    width: 70
  },
  {
    field: "delivered",
    title: "Delivered",
    editable: true,
    selectOptions: [
      { label: "true", value: "true" },
      { label: "false", value: "false" }
    ],
    editValue: val => {
      // convert to boolean type after onBlur on dropdown
      if (typeof val === "boolean") {
        return val;
      }
      return val === "true" ? true : val === "false" ? false : undefined;
    },
    width: 80
  },
  {
    field: "paid",
    title: "Paid",
    editable: false,
    defaultEditValue: false,
    width: 45
  },
  { field: "remark", title: "Remark", editable: true, width: 700 }
];

export const InlineEdit = () => {
  const [rows, setRows] = useState(data);
  const [showCode, setShowCode] = useState(false);

  return (
    <div>
      <EnhancedTable
        columns={columns}
        rows={rows}
        options={{
          height: "400px",
          multipleSelection: true
        }}
        editOptions={{
          onRowAdded: row => {
            setRows(rows => [...rows, row]);
          },
          onRowDeleted: (selectedRows, selectedTableDataIds) => {
            setRows(rows =>
              rows.filter((row, idx) => !selectedTableDataIds.includes(idx))
            );
          },
          onCellUpdated: (tableDataId, field, updatedValue) => {
            setRows(rows => {
              const newRows = [...rows];
              newRows[tableDataId] = {
                ...newRows[tableDataId],
                [field]: updatedValue
              };
              return newRows;
            });
          }
        }}
      />

      <button onClick={() => console.log(rows)}>show data</button>
      <br />
      <button onClick={() => setShowCode(!showCode)}>
        {showCode ? "hide" : "show"} code
      </button>

      {showCode && (
        <SyntaxHighlighter
          language="javascript"
          style={dark}
          customStyle={{ fontSize: "0.7em" }}
        >
          {code}
        </SyntaxHighlighter>
      )}
    </div>
  );
};
