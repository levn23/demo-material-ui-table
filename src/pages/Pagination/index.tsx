import {
  FormControl,
  FormLabel,
  RadioGroup,
  Radio,
  FormControlLabel
} from "@material-ui/core";
import React, { useState } from "react";
import { columns } from "../../columns";
import { data } from "../../data";
import { EnhancedTable } from "../../table/";
import { Prism as SyntaxHighlighter } from "react-syntax-highlighter";
import { dark } from "react-syntax-highlighter/dist/esm/styles/prism";

const code = `
export const DefaultTable = () => {
  const [rows, setRows] = useState(
    [
      { id: "1", orderNum: "QMH1001", orderDate: null, price: 11, delivered: false },
      { id: "3",  orderNum: "QMH1003", orderDate: moment("2021-02-22T00:00:00").toDate(), price: 15, delivered: true, remark: "long remark long remark long remark long remark long remark long remark long remark long remark long remark long remark long remark long remark long remark long remark " }
      // ...
    ]
  );

  return (
    <EnhancedTable
      columns={[ 
        { field: "id", title: "Id", width: 40 },
        { field: "orderNum", title: "Order Number", width: 140 },
        { field: "orderDate", title: "Order Date", type: "date", width: 100 },
        { field: "price", title: "Price", type: "number", align: "right", width: 70 },
        { field: "delivered", title: "Delivered", width: 55 },
        { field: "paid", title: "Paid", width: 70 },
        { field: "remark", title: "Remark", width: 700 }
      ]}
      rows={rows}
      options={{
        height: "300px",
        rowsPerPage: 25,
        rowsPerPageOptions: [25, 50, 75]
      }}
    />
  );
}`;

const opts = {
  opt1: [25, 50, 75],
  opt2: [30, 60, 90],
  opt3: [40, 80, 120]
};

export const Pagination = () => {
  const [rows, setRows] = useState(data);
  const [selectedOpt, setSelectedOpt] = useState("opt1");
  const [showCode, setShowCode] = useState(false);

  return (
    <div>
      <FormControl component="fieldset">
        <FormLabel component="legend">Row per page options</FormLabel>
        <RadioGroup
          name="gender1"
          value={selectedOpt}
          onChange={(evt, val) => setSelectedOpt(val)}
        >
          {Object.keys(opts).map(name => (
            <FormControlLabel
              key={name}
              value={name}
              control={<Radio />}
              label={`[${(opts as any)[name]}]`}
            />
          ))}
        </RadioGroup>
      </FormControl>
      <EnhancedTable
        columns={columns}
        rows={rows}
        options={{
          height: "300px",
          rowsPerPage: (opts as any)[selectedOpt][0],
          rowsPerPageOptions: (opts as any)[selectedOpt]
        }}
      />

      <button onClick={() => setShowCode(!showCode)}>
        {showCode ? "hide" : "show"} code
      </button>

      {showCode && (
        <SyntaxHighlighter
          language="javascript"
          style={dark}
          customStyle={{ fontSize: "0.7em" }}
        >
          {code}
        </SyntaxHighlighter>
      )}
    </div>
  );
};
