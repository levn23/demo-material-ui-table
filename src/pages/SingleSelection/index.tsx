import React, { useState } from "react";
import { Order } from "../../App";
import { columns } from "../../columns";
import { data } from "../../data";
import { EnhancedTable } from "../../table/";
import { Prism as SyntaxHighlighter } from "react-syntax-highlighter";
import { dark } from "react-syntax-highlighter/dist/esm/styles/prism";

const code = `
export const DefaultTable = () => {
  const [rows, setRows] = useState(
    [
      { id: "1", orderNum: "QMH1001", orderDate: null, price: 11, delivered: false },
      { id: "3",  orderNum: "QMH1003", orderDate: moment("2021-02-22T00:00:00").toDate(), price: 15, delivered: true, remark: "long remark long remark long remark long remark long remark long remark long remark long remark long remark long remark long remark long remark long remark long remark " }
      // ...
    ]
  );
  const [selectedRow, setSelectedRow] = useState<Order>();

  return (
    <EnhancedTable
      columns={[ 
        { field: "id", title: "Id", width: 40 },
        { field: "orderNum", title: "Order Number", width: 140 },
        { field: "orderDate", title: "Order Date", type: "date", width: 100 },
        { field: "price", title: "Price", type: "number", align: "right", width: 70 },
        { field: "delivered", title: "Delivered", width: 55 },
        { field: "paid", title: "Paid", width: 70 },
        { field: "remark", title: "Remark", width: 700 }
      ]}
      rows={rows}
      options={{
        height: "300px",
        onRowClick: (evt, row, tableDataId) => {
          setSelectedRow(row);
          alert(JSON.stringify(row));
        }
      }}
    />
  );
}`;

export const SingleSelection = () => {
  const [rows, setRows] = useState(data);
  const [selectedRow, setSelectedRow] = useState<Order>();
  const [showCode, setShowCode] = useState(false);

  return (
    <div>
      <EnhancedTable
        columns={columns}
        rows={rows}
        options={{
          height: "300px",
          onRowClick: (evt, row, tableDataId) => {
            setSelectedRow(row);
            alert(JSON.stringify(row));
          }
        }}
      />
      <button onClick={() => console.log(selectedRow)}>
        show selected data
      </button>
      <br />
      <button onClick={() => setShowCode(!showCode)}>
        {showCode ? "hide" : "show"} code
      </button>

      {showCode && (
        <SyntaxHighlighter
          language="javascript"
          style={dark}
          customStyle={{ fontSize: "0.7em" }}
        >
          {code}
        </SyntaxHighlighter>
      )}
    </div>
  );
};
