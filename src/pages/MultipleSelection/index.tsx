import React, { useState } from "react";
import { columns } from "../../columns";
import { data } from "../../data";
import { EnhancedTable } from "../../table/";
import { Prism as SyntaxHighlighter } from "react-syntax-highlighter";
import { dark } from "react-syntax-highlighter/dist/esm/styles/prism";

const code = `
export const DefaultTable = () => {
  const [rows, setRows] = useState(
    [
      { id: "1", orderNum: "QMH1001", orderDate: null, price: 11, delivered: false },
      { id: "3",  orderNum: "QMH1003", orderDate: moment("2021-02-22T00:00:00").toDate(), price: 15, delivered: true, remark: "long remark long remark long remark long remark long remark long remark long remark long remark long remark long remark long remark long remark long remark long remark " }
      // ...
    ]
  );
  const [selectedRows, setSelectedRows] = useState<typeof rows>([]);

  return (
    <EnhancedTable
      columns={[ 
        { field: "id", title: "Id", width: 40 },
        { field: "orderNum", title: "Order Number", width: 140 },
        { field: "orderDate", title: "Order Date", type: "date", width: 100 },
        { field: "price", title: "Price", type: "number", align: "right", width: 70 },
        { field: "delivered", title: "Delivered", width: 55 },
        { field: "paid", title: "Paid", width: 70 },
        { field: "remark", title: "Remark", width: 700 }
      ]}
      rows={rows}
      options={{
        height: "300px",
        multipleSelection: true,
        onSelectionChange: (rows, row) => {
          setSelectedRows(rows);
        }
      }}
    />
  );
}`;

export const MultipleSelection = () => {
  const [rows, setRows] = useState(data);
  const [selectedRows, setSelectedRows] = useState<typeof rows>([]);
  const [showCode, setShowCode] = useState(false);

  return (
    <div>
      <EnhancedTable
        columns={columns}
        rows={rows}
        options={{
          height: "300px",
          multipleSelection: true,
          onSelectionChange: (rows, row) => {
            setSelectedRows(rows);
          }
        }}
      />
      <button onClick={() => console.log(selectedRows)}>
        show selected data
      </button>
      <br />
      <button onClick={() => setShowCode(!showCode)}>
        {showCode ? "hide" : "show"} code
      </button>

      {showCode && (
        <SyntaxHighlighter
          language="javascript"
          style={dark}
          customStyle={{ fontSize: "0.7em" }}
        >
          {code}
        </SyntaxHighlighter>
      )}
    </div>
  );
};
