import { Snackbar, makeStyles } from "@material-ui/core";
import React, { useEffect, useRef, useState } from "react";
import { useHotkeys } from "react-hotkeys-hook";

const useStyle = makeStyles({
  table: {
    borderCollapse: "collapse",
    "& td,th": {
      border: "1px solid rgba(224,224,224,1)"
    }
  }
});

interface AlertProps {
  text: string;
  handleClose: () => void;
}

const Alert = ({ text, handleClose }: AlertProps) => {
  return (
    <Snackbar
      open
      onClose={handleClose}
      message={text}
      anchorOrigin={{ vertical: "top", horizontal: "center" }}
    />
  );
};

export const TestShortcutKey = () => {
  const classes = useStyle();
  const [alertState, setAlertState] = useState<AlertProps>();
  const containerRef = useRef<HTMLDivElement>(null);

  const showAlert = (e: KeyboardEvent, text: string) => {
    if (containerRef.current?.contains(document.activeElement)) {
      e.stopPropagation();
      e.preventDefault();
      setAlertState({ text, handleClose: () => setAlertState(undefined) });
    }
  };

  // use dom onkeydown with useEffect
  // useEffect(() => {
  //   if (containerRef?.current) {
  //     containerRef.current.onkeydown = e => {
  //       console.log(e.key);
  //       if (e.ctrlKey) {
  //         // Ctrl + 0 to 9
  //         if (Array.from({ length: 10 }, (v, k) => k + "").includes(e.key)) {
  //           showAlert(e, `Ctrl + ${e.key}`);
  //         }
  //         // Ctrl + Insert
  //         e.key === "Insert" && showAlert(e, "Ctrl + Insert");
  //         // Ctrl + Delete
  //         e.key === "Delete" && showAlert(e, "Ctrl + Delete");
  //         // Ctrl + Spacebar
  //         e.key === " " && showAlert(e, "Ctrl + Spacebar");
  //         // Ctrl + End
  //         e.key === "End" && showAlert(e, "Ctrl + End");
  //         // Ctrl + PageUp (not work in Chrome)
  //         e.key === "PageUp" && showAlert(e, "Ctrl + PageUp");
  //         // Ctrl + PageDown (not work in Chrome)
  //         e.key === "PageDown" && showAlert(e, "Ctrl + PageDown");
  //       }

  //       // Insert
  //       e.key === "Insert" && showAlert(e, "Insert");
  //       // Tab
  //       e.key === "Tab" && showAlert(e, "Tab");
  //       // Shift + Tab
  //       e.shiftKey && e.key === "Tab" && showAlert(e, "Shift + Tab");
  //     };
  //   }
  // }, []);

  // use react-hotkeys-hook
  // !!!important: to use number pad <= v3.2.1, need to add devDependencies with hotkeys-js v3.8.3
  useHotkeys("ctrl+0,ctrl+num_0", evt => showAlert(evt, "Ctrl + 0"));
  useHotkeys("ctrl+1,ctrl+num_1", evt => showAlert(evt, "Ctrl + 1"));
  useHotkeys("ctrl+2,ctrl+num_2", evt => showAlert(evt, "Ctrl + 2"));
  useHotkeys("ctrl+3,ctrl+num_3", evt => showAlert(evt, "Ctrl + 3"));
  useHotkeys("ctrl+4,ctrl+num_4", evt => showAlert(evt, "Ctrl + 4"));
  useHotkeys("ctrl+5,ctrl+num_5", evt => showAlert(evt, "Ctrl + 5"));
  useHotkeys("ctrl+6,ctrl+num_6", evt => showAlert(evt, "Ctrl + 6"));
  useHotkeys("ctrl+7,ctrl+num_7", evt => showAlert(evt, "Ctrl + 7"));
  useHotkeys("ctrl+8,ctrl+num_8", evt => showAlert(evt, "Ctrl + 8"));
  useHotkeys("ctrl+9,ctrl+num_9", evt => showAlert(evt, "Ctrl + 9"));
  useHotkeys("ctrl+insert", evt => showAlert(evt, "Ctrl + INSERT"));
  useHotkeys("ctrl+del", evt => showAlert(evt, "Ctrl + DELETE"));
  useHotkeys("insert", evt => showAlert(evt, "insert"));
  useHotkeys("ctrl+space", evt => showAlert(evt, "Ctrl + Spacebar"));
  useHotkeys("ctrl+pageup", evt => showAlert(evt, "Ctrl + Page Up"));
  useHotkeys("ctrl+pagedown", evt => showAlert(evt, "Ctrl + Page Down"));
  useHotkeys("ctrl+end", evt => showAlert(evt, "Ctrl + End"));
  useHotkeys("tab", evt => showAlert(evt, "Tab"));
  useHotkeys("shift+tab", evt => showAlert(evt, "Shift + Tab"));
  useHotkeys("up", evt => showAlert(evt, "Up"));
  useHotkeys("down", evt => showAlert(evt, "Down"));
  useHotkeys("left", evt => showAlert(evt, "Left"));
  useHotkeys("right", evt => showAlert(evt, "Right"));

  return (
    <div>
      <div
        ref={containerRef}
        tabIndex={-1}
        style={{ outline: "none", height: "600px", backgroundColor: "#ccc" }}
      >
        <h3>focus this grey area</h3>
        <table className={classes.table} cellPadding={0}>
          <thead>
            <tr>
              <th>Short-cut keys</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Ctrl + 0</td>
            </tr>
            <tr>
              <td>Ctrl + 1</td>
            </tr>
            <tr>
              <td>Ctrl + 2</td>
            </tr>
            <tr>
              <td>Ctrl + 3</td>
            </tr>
            <tr>
              <td>Ctrl + 4</td>
            </tr>
            <tr>
              <td>Ctrl + 5</td>
            </tr>
            <tr>
              <td>Ctrl + 6</td>
            </tr>
            <tr>
              <td>Ctrl + 7</td>
            </tr>
            <tr>
              <td>Ctrl + 8</td>
            </tr>
            <tr>
              <td>Ctrl + 9</td>
            </tr>
            <tr>
              <td>Ctrl + INSERT</td>
            </tr>
            <tr>
              <td>Ctrl + DELETE</td>
            </tr>
            <tr>
              <td>INSERT</td>
            </tr>
            <tr>
              <td>Ctrl + Spacebar</td>
            </tr>
            <tr>
              <td style={{ color: "red" }}>Ctrl + Page Up</td>
            </tr>
            <tr>
              <td style={{ color: "red" }}>Ctrl + Page Down</td>
            </tr>
            <tr>
              <td>Ctrl + End</td>
            </tr>
            <tr>
              <td>Tab</td>
            </tr>
            <tr>
              <td>Shift + Tab</td>
            </tr>
          </tbody>
        </table>
      </div>
      {alertState && <Alert {...alertState} />}
    </div>
  );
};
