import { Checkbox } from "@material-ui/core";
import React, { useMemo, useState } from "react";
import { Order } from "../../App";
import { data } from "../../data";
import { Column, EnhancedTable } from "../../table/";
import { Prism as SyntaxHighlighter } from "react-syntax-highlighter";
import { dark } from "react-syntax-highlighter/dist/esm/styles/prism";

const code = `
export const DefaultTable = () => {
  const [rows, setRows] = useState(
    [
      { id: "1", orderNum: "QMH1001", orderDate: null, price: 11, delivered: false },
      { id: "3",  orderNum: "QMH1003", orderDate: moment("2021-02-22T00:00:00").toDate(), price: 15, delivered: true, remark: "long remark long remark long remark long remark long remark long remark long remark long remark long remark long remark long remark long remark long remark long remark " }
      // ...
    ]
  );
  const columns: Column<Order>[] = useMemo(
    () => [
      { field: "id", title: "Id", width: 40 },
      {
        field: "test",
        title: "Custom Checkbox",
        render: (row, tableDataId) => (
          <Checkbox
            size="small"
            style={{ padding: 0 }}
            checked={row.delivered}
            onChange={evt =>
              setRows(rows => {
                const newRows = [...rows];
                newRows[tableDataId] = {
                  ...newRows[tableDataId],
                  delivered: evt.target.checked
                };
                return newRows;
              })
            }
          />
        ),
        width: 70
      },
      { field: "orderNum", title: "Order Number", width: 140 },
      { field: "orderDate", title: "Order Date", type: "date", width: 100 },
      { field: "price",  title: "Price",  type: "number", align: "right", width: 70 },
      { field: "delivered", title: "Delivered", width: 55 },
      { field: "paid", title: "Paid", width: 70 },
      { field: "remark", title: "Remark",  width: 700 }
    ],
    []
  );

  return (
    <EnhancedTable
      columns={columns}
      rows={rows}
      options={{ height: "300px" }}
    />
  );
}`;

export const CustomColumnTable = () => {
  const [rows, setRows] = useState(data);
  const [showCode, setShowCode] = useState(false);

  const columns: Column<Order>[] = useMemo(
    () => [
      { field: "id", title: "Id", width: 40 },
      {
        field: "test",
        title: "Custom Checkbox",
        render: (row, tableDataId) => (
          <Checkbox
            size="small"
            style={{ padding: 0 }}
            checked={row.delivered}
            onChange={evt =>
              setRows(rows => {
                const newRows = [...rows];
                newRows[tableDataId] = {
                  ...newRows[tableDataId],
                  delivered: evt.target.checked
                };
                return newRows;
              })
            }
          />
        ),
        width: 70
      },
      { field: "orderNum", title: "Order Number", width: 140 },
      { field: "orderDate", title: "Order Date", type: "date", width: 100 },
      {
        field: "price",
        title: "Price",
        type: "number",
        align: "right",
        width: 70
      },
      { field: "delivered", title: "Delivered", width: 55 },
      { field: "paid", title: "Paid", width: 70 },
      { field: "remark", title: "Remark", width: 700 }
    ],
    []
  );

  return (
    <div>
      <div style={{ marginBottom: "20px" }}>
        <div>Custom "Custom Checkbox" column</div>
        <div>Click checkbox will update "Delivered" value</div>
      </div>

      <EnhancedTable
        columns={columns}
        rows={rows}
        options={{ height: "300px" }}
      />
      <button onClick={() => console.log(rows)}>show data</button>
      <br />
      <button onClick={() => setShowCode(!showCode)}>
        {showCode ? "hide" : "show"} code
      </button>

      {showCode && (
        <SyntaxHighlighter
          language="javascript"
          style={dark}
          customStyle={{ fontSize: "0.7em" }}
        >
          {code}
        </SyntaxHighlighter>
      )}
    </div>
  );
};
