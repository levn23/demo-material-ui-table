import React from "react";
import "./App.css";
import { Link, Switch, Route } from "react-router-dom";
import { Container } from "@material-ui/core";
import { DefaultTable } from "./pages/Default";
import { MultipleSelection } from "./pages/MultipleSelection";
import { SingleSelection } from "./pages/SingleSelection";
import { RowDoubleClick } from "./pages/RowDoubleClick";
import { Pagination } from "./pages/Pagination";
import { InlineEdit } from "./pages/InlineEdit";
import { CustomColumnTable } from "./pages/CustomColumn";
import { TestShortcutKey } from "./pages/TestShortcutKey";

export type Order = {
  id: string;
  orderNum: string;
  orderDate?: Date | null;
  price?: number;
  delivered: boolean;
  paid: boolean;
  remark?: string;
};

function App() {
  return (
    <div>
      <nav>
        <Link to="/">Default</Link>
        <Link to="/multiple-selection">Multiple Selection</Link>
        <Link to="/single-selection">Single Selection</Link>
        <Link to="/row-double-click">Row Double Click</Link>
        <Link to="/pagination">Pagination</Link>
        <Link to="/custom-column">Custom Column</Link>
        <Link to="/inline-edit">🚧 Inline Edit</Link>
        <Link to="/shortcut-keys">Test Short-cut Keys</Link>
      </nav>

      <hr />

      <Container>
        <Switch>
          <Route path="/multiple-selection">
            <MultipleSelection />
          </Route>
          <Route path="/single-selection">
            <SingleSelection />
          </Route>
          <Route path="/row-double-click">
            <RowDoubleClick />
          </Route>
          <Route path="/pagination">
            <Pagination />
          </Route>
          <Route path="/inline-edit">
            <InlineEdit />
          </Route>
          <Route path="/custom-column">
            <CustomColumnTable />
          </Route>
          <Route path="/shortcut-keys">
            <TestShortcutKey />
          </Route>

          <Route path="/">
            <DefaultTable />
          </Route>
        </Switch>
      </Container>
    </div>
  );
}

export default App;
