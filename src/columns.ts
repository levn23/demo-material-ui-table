import { Order } from "./App";
import { Column } from "./table";

export const columns: Column<Order>[] = [
  { field: "id", title: "Id", width: 40 },
  { field: "orderNum", title: "Order Number", width: 140 },
  { field: "orderDate", title: "Order Date", type: "date", width: 100 },
  { field: "price", title: "Price", type: "number", align: "right", width: 70 },
  { field: "delivered", title: "Delivered", width: 80 },
  { field: "paid", title: "Paid", width: 55 },
  { field: "remark", title: "Remark", width: 700 }
];
